package feladat0_maven;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import feladat0_maven.AccountValidator;

public class AccountValidatorTest {

	AccountValidator av = new AccountValidator();
	
	@Test
	public void isValidUSerName_onLessThan6Char_returnFalse() throws Exception {
		
		boolean result = av.isValid("hisof");
		assertFalse(result);
	}
	@Test
	public void isValidUSerName_on6Char_returnTrue() throws Exception {
		
		boolean result = av.isValid("hisofh");
		assertTrue(result);
	}
	@Test
	public void isValidUSerName_onMoreThan6Char_returnTrue() throws Exception {
		
		boolean result = av.isValid("hisofhklkkli");
		assertTrue(result);
	}
	@Test
	public void isValidUSerName_onLessThan6CharIncludeSpace_returnFalse() throws Exception {
		
		boolean result = av.isValid("hi h");
		assertFalse(result);
	}
	@Test
	public void isValidUSerName_onMoreThan6CharIncludeSpace_returnFalse() throws Exception {
		
		boolean result = av.isValid("hifjslfjsjfalda h");
		assertFalse(result);
	}
	@Test
	public void isValidUSerName_onNullParameter_returnNullExceptionMessage(){
		
		/*try {
			av.isValid(null);
		} catch (Exception e) {
			assertEquals("isValid() method must get a userName as a string!", e.getMessage());
		}*/
		assertThrows(Exception.class, () -> {
			av.isValid(null); 
		} );
	}


}
