package feladat0_maven;

import feladat0_maven.User;
import feladat0_maven.UserService;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class UserServiceTest {
	
	UserService us = new UserService();

	@Test
	public void testSaveMethod_onEarlierValidatedUserObject_resultEqualsOK() throws Exception {
		
		User user1 = new User("fnsfnskjfnskjf"); //validated earlier
		try {
			us.save(user1);
			assertEquals("Mentve: fnsfnskjfnskjf", "Mentve: " + user1.getUserName());
			//don't throw exception - just check the object creation
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	@Test
	public void testSaveMethod_onNullObjectInsteadaUserObject_resultExceptionMessage() {
		
		
		assertThrows(Exception.class, () -> {
			User user1 = new User(null); //directly NULL input for User object - This will throw exception
			us.save(user1);
		});
	}

}
