package feladat0_maven;

import feladat0_maven.JsInjectionValidator;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class JsInjectionValidatorTest {
	
	JsInjectionValidator jsiv = new JsInjectionValidator();

	@Test
	public void isValidUserName_onMoreThan6CharIncludeJS_returnFalse() throws Exception {
		
		boolean result = jsiv.isValid("hisofkhkkhkjh<script>");
		assertFalse(result);
	}
	@Test
	public void isValidUserName_onLessThan6CharIncludeJS_returnFalse() throws Exception {
		
		boolean result = jsiv.isValid("his<script>d");
		assertFalse(result);
	}
	@Test
	public void isValidUserName_onMoreThan6CharWithoutJS_returnTrue() throws Exception {
		
		boolean result = jsiv.isValid("hisofkhkkhkjh");
		assertTrue(result);
	}	
	@Test
	public void isValidUserName_onNullParameter_returnNullExceptionMessage() {
		
		assertThrows(Exception.class, () -> {
			jsiv.isValid(null); 
		} );

			
	}

}
