package feladat0_maven;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class AccountValidatorTestMock {
	
	AccountValidator av = Mockito.mock(AccountValidator.class);
	@Test
	public void testIsValid_onMoreThan6Char_returnValid() throws Exception {
		Mockito.when(av.isValid("gfdgdgdgdg")).thenReturn(true);
		System.out.println("gfdgdgdgdg validation result: " + av.isValid("gfdgdgdgdg"));
		System.out.println("abc validation result: " + av.isValid("abc"));
		Mockito.verify(av).isValid("gfdgdgdgdg");
		
	}
	@Test
	public void testIsValid_onLessThan6Char_returnInValid() throws Exception {
		Mockito.when(av.isValid("abc")).thenReturn(false);
		System.out.println("abc validation result: " + av.isValid("abc"));
		
		Mockito.verify(av).isValid("abc");
	}
	@Test
	public void testIsValid_onMoreThan6CharIncludeSpace_returnInValid() throws Exception {
		Mockito.when(av.isValid("fhsfgsjhfs fsfs")).thenReturn(false);
		System.out.println("fhsfgsjhfs fsfs validation result: " + av.isValid("fhsfgsjhfs fsfs"));
		
		Mockito.verify(av).isValid("fhsfgsjhfs fsfs");
	}

}
