package feladat0_maven;

import java.util.ArrayList;
import java.util.List;

import feladat0_maven.AccountValidator;
import feladat0_maven.IValidator;
import feladat0_maven.JsInjectionValidator;
import feladat0_maven.User;
import feladat0_maven.UserController;
import feladat0_maven.UserService;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

public class UserControllerTest {
	
	AccountValidator av = new AccountValidator();
	JsInjectionValidator jsiv = new JsInjectionValidator();
	UserService us = new UserService();
	List <IValidator> allValidators = new ArrayList<IValidator>();
	

	@Test
	public void createUserTest_onNullParameter_returnNullExceptionMessage() {
		
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		assertThrows(Exception.class, () -> {
			uc.createUser(null);
		});
	}
	@Test
	public void createUserTest_onAllValidatorsPassedParameter_returnSuccessfullySavedObjectWithUserName() throws Exception {
		
		allValidators.add(av);
		allValidators.add(jsiv);
		User user1 = new User("rzwiurzuwhewiuf");
		UserController uc = new UserController(allValidators,us);
		
		try {
			uc.createUser(user1.getUserName());
			assertEquals("rzwiurzuwhewiuf", user1.getUserName());
			//don't throw exception - just check the object creation

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void validUserNameTest_onNullParameter_returnValidatorGotNullMessage() {
		
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		assertThrows(Exception.class, () -> {
			uc.validUserName(null);
		} );
		
	}
	@Test
	public void validUserNameTest_onMoreThan6CharWithSpaceIncluded_returnAccountValidatorIsValidFalse() throws Exception {
		
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("fsfhsiofhsoif f");
		assertFalse(bool);
		
	}
	@Test
	public void validUserNameTest_onLassThan6Char_returnAccountValidatorIsValidFalse() throws Exception {
		
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("hiuf");
		assertFalse(bool);
		
	}
	@Test
	public void validUserNameTest_onMoreThan6CharWithoutSpace_returnAccountValidatorIsValidTrue() throws Exception {
		
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("hifsjfkjuf");
		assertTrue(bool);
		
	}
	@Test
	public void validUserNameTest_onMoreThan6CharWithoutSpaceButJSIncluded_returnJSInjectionValidatorIsValidFalse() throws Exception {
		
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("hifsjf<script>kjuf");
		assertFalse(bool);
		
	}

}
