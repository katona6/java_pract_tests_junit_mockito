package feladat0_maven;
import java.util.ArrayList;
import java.util.List;

public class Main
{

	public static void main(String[] args) 
	{
			String userNamesArray[] = new String[6];
			userNamesArray[0] = "cccccccccc";
			userNamesArray[1] = "aaaaaaaaaa";
			userNamesArray[2] = "bbbbbbbbbb b";
			userNamesArray[3] = "ddd";
			userNamesArray[4] = "eee e";
			userNamesArray[5] = "fffffffffffff<script>";
			
			//System.out.println("Konstruktor tesztelése...");
			//UserController u1 = new UserController(IValidator.class.getInterfaces());
			AccountValidator av = new AccountValidator();
			JsInjectionValidator jsiv = new JsInjectionValidator();
			UserService us = new UserService();
			
			List <IValidator> allValidators = new ArrayList<IValidator>();
			allValidators.add(av);
			allValidators.add(jsiv);
			
			UserController uc = new UserController(allValidators, us );
			try {
				for (int i=0; i < userNamesArray.length; i++)
				{
					System.out.println("Kapott " + i + ". usernév: "+ userNamesArray[i]);
					if(uc.validUserName(userNamesArray[i]))
					{
						System.out.println(userNamesArray[i] + " valid");
						uc.createUser(userNamesArray[i]);
					} else {
						throw new Exception(userNamesArray[i] + " NOT valid \nTovábbi feldolgozás megszakadt. \nProgram vége...");
					}
					
				}
			} catch (Exception uc_UserControllerException) {
				uc_UserControllerException.printStackTrace();
			}
			System.out.println("\nFeldolgozás lezajlott.\nProgram vége.");
		

	} //end of main()

} //end of Class
