package feladat0_maven;

public class User {
	private String userName;		//readonly username

	public User(String userName) throws Exception {				//for this case usage -- should be without parameter+setter for Spring later...
		if(userName == null) {
			throw new Exception("User object creation cannot be with NULL userName!");
		} else {
			this.userName = userName;
		}
	}
	
	public void setUserName(String userName) {	//for Spring
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}
	
	

}
