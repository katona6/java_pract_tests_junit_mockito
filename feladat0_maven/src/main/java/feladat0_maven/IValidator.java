package feladat0_maven;

public interface IValidator 
{
	public boolean isValid(String userName) throws Exception;

}
