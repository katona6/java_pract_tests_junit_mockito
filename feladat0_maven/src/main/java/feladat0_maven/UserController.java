package feladat0_maven;

import java.util.List;

public class UserController
{

	private List<IValidator> validList;
	private UserService us;
	
	public UserController(List<IValidator> validators, UserService us) {
		//konstruktor
		//ami függőségeket fogadja, és menti adattagba ( Validátorok listája, UserService)
		
		this.validList = validators;
		this.us = us;
		

	}


	public String getValidList() {
		return validList.toString();
		
	}
	
	

	public void createUser(String username) throws Exception {
		//ezt hivják majd kivülröl
		//UserService.save(new User(username));
		if(this.validUserName(username)) {
			us.save(new User(username));
			
		}
	}
	
	public boolean validUserName(String userName) throws Exception{
		//használja a validátorokat
		//ha bármelyik validátor elbukik, akkor a bemenet érvénytelen
		boolean valid = true;

		for (IValidator iValidator : validList) {
			
			if (iValidator.isValid(userName) == false){
				valid = false;
				break;
			}
		}
		return valid;
		
	}
	
}
