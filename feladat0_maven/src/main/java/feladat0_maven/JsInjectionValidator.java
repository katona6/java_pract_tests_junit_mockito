package feladat0_maven;

public class JsInjectionValidator implements IValidator
{
	public boolean isValid(String userName) throws Exception
	{
		if (userName == null) {
			throw new Exception("isValid() method must get a userName as a string!");
		}
		if(userName.indexOf("<script>") == -1) { //check contains a script
			return true;
		} else {			
			return false;
		}
	}
}
