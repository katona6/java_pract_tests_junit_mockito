package feladat0.TESTS;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import feladat0.AccountValidator;

public class AccountValidatorTest {
	
	@Test
	public void isValidUSerName_onLessThan6Char_returnFalse() throws Exception {
		AccountValidator av = new AccountValidator();
		boolean result = av.isValid("hisof");
		assertFalse(result);
	}
	@Test
	public void isValidUSerName_on6Char_returnTrue() throws Exception {
		AccountValidator av = new AccountValidator();
		boolean result = av.isValid("hisofh");
		assertTrue(result);
	}
	@Test
	public void isValidUSerName_onMoreThan6Char_returnTrue() throws Exception {
		AccountValidator av = new AccountValidator();
		boolean result = av.isValid("hisofhklkkli");
		assertTrue(result);
	}
	@Test
	public void isValidUSerName_onLessThan6CharIncludeSpace_returnFalse() throws Exception {
		AccountValidator av = new AccountValidator();
		boolean result = av.isValid("hi h");
		assertFalse(result);
	}
	@Test
	public void isValidUSerName_onMoreThan6CharIncludeSpace_returnFalse() throws Exception {
		AccountValidator av = new AccountValidator();
		boolean result = av.isValid("hifjslfjsjfalda h");
		assertFalse(result);
	}
	@Test
	public void isValidUSerName_onNullParameter_returnNullExceptionMessage() throws Exception {
		AccountValidator av = new AccountValidator();
		try {
			av.isValid(null);
		} catch (Exception e) {
			assertEquals("isValid() method must get a userName as a string!", e.getMessage());
		}
	}

}
