package feladat0.TESTS;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import feladat0.AccountValidator;
import feladat0.IValidator;
import feladat0.JsInjectionValidator;
import feladat0.User;
import feladat0.UserController;
import feladat0.UserService;

class UserControllerTest {

	@Test
	void createUserTest_onNullParameter_returnNullExceptionMessage() {
		AccountValidator av = new AccountValidator();
		JsInjectionValidator jsiv = new JsInjectionValidator();
		UserService us = new UserService();
		List <IValidator> allValidators = new ArrayList<IValidator>();
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		try {
			uc.createUser(null);
		} catch (Exception e) {
			assertEquals("isValid() method must get a userName as a string!", e.getMessage());
		}
	}
	@Test
	void createUserTest_onAllValidatorsPassedParameter_returnSuccessfullySavedObjectWithUserName() {
		AccountValidator av = new AccountValidator();
		JsInjectionValidator jsiv = new JsInjectionValidator();
		UserService us = new UserService();
		List <IValidator> allValidators = new ArrayList<IValidator>();
		allValidators.add(av);
		allValidators.add(jsiv);
		User user1 = new User("rzwiurzuwhewiuf");
		UserController uc = new UserController(allValidators,us);
		
		try {
			uc.createUser(user1.getUserName());
			assertEquals("rzwiurzuwhewiuf", user1.getUserName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	void validUserNameTest_onNullParameter_returnValidatorGotNullMessage() {
		AccountValidator av = new AccountValidator();
		JsInjectionValidator jsiv = new JsInjectionValidator();
		UserService us = new UserService();
		List <IValidator> allValidators = new ArrayList<IValidator>();
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		try {
			uc.validUserName(null);
		} catch (Exception e) {
			assertEquals("isValid() method must get a userName as a string!", e.getMessage());
		}
		
	}
	@Test
	void validUserNameTest_onMoreThan6CharWithSpaceIncluded_returnAccountValidatorIsValidFalse() throws Exception {
		AccountValidator av = new AccountValidator();
		JsInjectionValidator jsiv = new JsInjectionValidator();
		UserService us = new UserService();
		List <IValidator> allValidators = new ArrayList<IValidator>();
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("fsfhsiofhsoif f");
		assertFalse(bool);
		
	}
	@Test
	void validUserNameTest_onLassThan6Char_returnAccountValidatorIsValidFalse() throws Exception {
		AccountValidator av = new AccountValidator();
		JsInjectionValidator jsiv = new JsInjectionValidator();
		UserService us = new UserService();
		List <IValidator> allValidators = new ArrayList<IValidator>();
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("hiuf");
		assertFalse(bool);
		
	}
	@Test
	void validUserNameTest_onMoreThan6CharWithoutSpace_returnAccountValidatorIsValidTrue() throws Exception {
		AccountValidator av = new AccountValidator();
		JsInjectionValidator jsiv = new JsInjectionValidator();
		UserService us = new UserService();
		List <IValidator> allValidators = new ArrayList<IValidator>();
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("hifsjfkjuf");
		assertTrue(bool);
		
	}
	@Test
	void validUserNameTest_onMoreThan6CharWithoutSpaceButJSIncluded_returnJSInjectionValidatorIsValidFalse() throws Exception {
		AccountValidator av = new AccountValidator();
		JsInjectionValidator jsiv = new JsInjectionValidator();
		UserService us = new UserService();
		List <IValidator> allValidators = new ArrayList<IValidator>();
		allValidators.add(av);
		allValidators.add(jsiv);
		UserController uc = new UserController(allValidators,us);
		
		boolean bool = uc.validUserName("hifsjf<script>kjuf");
		assertFalse(bool);
		
	}

}
