package feladat0.TESTS;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import feladat0.User;
import feladat0.UserService;

public class UserServiceTest {

	@Test
	public void testSaveMethod_onEarlierValidatedUserObject_resultEqualsOK() {
		UserService us = new UserService();
		User user1 = new User("fnsfnskjfnskjf"); //validated earlier
		try {
			us.save(user1);
			assertEquals("Mentve: fnsfnskjfnskjf", "Mentve: " + user1.getUserName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	@Test
	public void testSaveMethod_onNullObjectInsteadaUserObject_resultExceptionMessage() {
		UserService us = new UserService();
		User user1 = new User(null); //directly NULL input for User object
		try {
			us.save(user1);
		} catch (Exception e) {
			assertEquals("save() method don't have a User Object!", e.getMessage());
		}
	}
	
	
}
