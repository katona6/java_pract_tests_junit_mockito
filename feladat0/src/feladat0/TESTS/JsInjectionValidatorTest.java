package feladat0.TESTS;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import feladat0.JsInjectionValidator;

public class JsInjectionValidatorTest {

	@Test
	public void isValidUserName_onMoreThan6CharIncludeJS_returnFalse() throws Exception {
		JsInjectionValidator jsiv = new JsInjectionValidator();
		boolean result = jsiv.isValid("hisofkhkkhkjh<script>");
		assertFalse(result);
	}
	@Test
	public void isValidUserName_onLessThan6CharIncludeJS_returnFalse() throws Exception {
		JsInjectionValidator jsiv = new JsInjectionValidator();
		boolean result = jsiv.isValid("his<script>d");
		assertFalse(result);
	}
	@Test
	public void isValidUserName_onMoreThan6CharWithoutJS_returnTrue() throws Exception {
		JsInjectionValidator jsiv = new JsInjectionValidator();
		boolean result = jsiv.isValid("hisofkhkkhkjh");
		assertTrue(result);
	}	
	@Test
	public void isValidUserName_onNullParameter_returnNullExceptionMessage() throws Exception {
		JsInjectionValidator jsiv = new JsInjectionValidator();
		
		try {
			jsiv.isValid(null);
		} catch (Exception e) {
			assertEquals("isValid() method must get a userName as a string!", e.getMessage());
		}
		
		
	}

}
