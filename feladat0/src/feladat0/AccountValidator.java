package feladat0;

public class AccountValidator implements IValidator
{	
	public boolean isValid(String userName) throws Exception
	{
		if (userName == null) {
			throw new Exception("isValid() method must get a userName as a string!");
		}
		if((userName.length() > 5) && (userName.indexOf(" ") == -1)) { //check minimum 6 characters and space
			return true;
		} else {			
			return false;
		}
	}

}
